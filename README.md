# Distributed TEE Service

- The **tee_distributed_tee_service** project supports exposing the security capabilities of a single device to serve the security computing needs of all scenarios. You can choose to run enclave on a remote device.
- This project provides specific usage examples.
  - **CallRemoteEnclaveService** directly calls the security service provided by the remote device.
  - **MigrateLocalEnclaveService** deploys the local TEE on the remote device and completes subsequent operations.