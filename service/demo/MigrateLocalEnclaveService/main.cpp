/*
 * Copyright (c) 2023 IPADS, Shanghai Jiao Tong University.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <bitset>
#include <cstdint>
#include <cstdio>
#include <cstring>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <string>

#include "DDSServer.h"
#include "Softbus.h"

#include <cstdlib>
#include <pthread.h>
#include "PLEnclaveEngine.h"


using namespace eprosima::fastrtps;
using namespace eprosima::fastrtps::rtps;
using std::cout;
using std::endl;
using namespace enclaveelf;
using namespace demoparam;
using namespace penglaienclave;

enum class E_SIDE {
    CLIENT,
    SERVER
};

struct args {
    char *in;
    int i;
};

static int check_args(E_SIDE *side, int argc, char **argv, int samples)
{
    if (argc > ARG_ONE) {
        if (strcmp(argv[ARG_ONE], "client") == 0) {
            *side = E_SIDE::CLIENT;
        } else if (strcmp(argv[ARG_ONE], "server") == 0) {
            *side = E_SIDE::SERVER;
        } else {
            cout << "Argument 1 needs to be client OR server" << endl;
            return 0;
        }
    } else {
        cout << "Client Server Test needs 1 arguments: (client/server)" << endl;
        return 0;
    }
}

int main(int argc, char **argv)
{
    cout << "Starting " << endl;
    E_SIDE side;
    int samples;

    if (!check_args(&side, argc, argv, samples)) {
        return 0;
    };

    if (side == E_SIDE::SERVER) {
        SoftbusServer server;
        server.publish_service("Enclave_operation_remote",
                               PLEnclaveEngine::enclave_operation_remote);
        server.run();
    }
    if (side == E_SIDE::CLIENT) {
        struct elf_args *enclaveFile =
            static_cast<elf_args *>(malloc(sizeof(struct elf_args)));

        char *eappfile = argv[ARG_TWO];
        elf_args_init(enclaveFile, eappfile);
        if (!elf_valid(enclaveFile)) {
            printf("error when initializing enclaveFile\n");
        }

        struct Enclave *enclave =
            static_cast<Enclave *>(malloc(sizeof(struct Enclave)));

        struct enclave_args *params =
            static_cast<enclave_args *>(malloc(sizeof(struct enclave_args)));
        if (enclaveFile == nullptr || params == nullptr || enclave == nullptr) {
            return -1;
        }
        enclave->type = REMOTE_ENCLAVE;

        enclave_param_init(params);
        PLEnclaveEngine engine =
            PLEnclaveEngine(nullptr, enclave, enclaveFile, params);
        params->untrusted_mem_size = DEFAULT_UNTRUSTED_SIZE;
        params->untrusted_mem_ptr = 0;
        engine.enclave_init();
        if (engine.enclave_create() < 0) {
            printf("host: failed to create enclave\n");
        } else {
            engine.enclave_attest(NONCE);

            engine.enclave_run();
        }
        engine.enclave_finalize();
    }

    cout << "EVERYTHING STOPPED FINE" << endl;
}
