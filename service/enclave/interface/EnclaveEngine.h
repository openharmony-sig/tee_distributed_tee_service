/*
 * Copyright (c) 2023 IPADS, Shanghai Jiao Tong University.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef EnclaveEngine_H
#define EnclaveEngine_H

#include "string.h"
#include "stdarg.h"

class EnclaveEngine {
public:
    EnclaveEngine(){};
    ~EnclaveEngine(){};

    virtual void enclave_init() = 0;
    virtual void enclave_finalize() = 0;
    virtual int enclave_create() = 0;
    virtual int enclave_run() = 0;
    virtual int enclave_stop() = 0;
    virtual int enclave_resume() = 0;
    virtual int enclave_destroy() = 0;
    virtual int enclave_attest() = 0;

private:
};

#endif