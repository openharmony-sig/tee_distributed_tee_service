## Enclave Architecture

### Code Structure

The main current Enclave interfaces are in the . /encalve/interfaces directory, and there can be multiple specific enclave implementations in tee_framework as well. You can also replace the penglai enclave with other enclaves by registering changes to the corresponding interfaces in xxxx-EnclaveEngine.h.

```bash
/enclave/
├── README.md
├── interface
│   └── EnclaveEngine.h
└── tee_framework
    └── penglai
        ├── include
        │   ├── PLEnclaveEngine.h
        │   ├── PLparam.h
        │   ├── elf.h
        │   ├── enclave_demo
        │   │   ├── face_recognition_shared_param.h
        │   │   └── run_enclave.h
        │   └── penglai-enclave.h
        └── src
            ├── PLEnclaveEngine.cpp
            ├── PLparam.cpp
            ├── elf.cpp
            ├── enclave_demo
            │   ├── Makefile
            │   ├── face_recognition_entry.c
            │   └── run_enclave.cpp
            └── penglai-enclave.cpp
```



### Enclave Interface

The interface that currently manages the enclave:

```c++
    virtual void enclave_init() = 0;
    virtual void enclave_finalize() = 0;
    virtual int enclave_create() = 0;
    virtual int enclave_run() = 0;
    virtual int enclave_stop() = 0;
    virtual int enclave_resume() = 0;
    virtual int enclave_destroy() = 0;
    virtual int enclave_attest() = 0;
```

You don't have to implement all the interfaces, but at least you need to support creating, running, and stopping Enclaves.

### How to use other enclave implementations

1. Create a directory in tee_framewore identical to the penglai directory, including header files and implementations. You need to implement the enclave interface that corresponds to the Enclave Interface in directory.
`enclave/tee_framework/penglai/include/PLEnclaveEngine.h`
    ```c++
    class PLEnclaveEngine:public EnclaveEngine{
        /*the enclave interface*/
        void enclave_init();
        void enclave_finalize();
        int enclave_create();
        int enclave_run();
        int enclave_stop();
        int enclave_resume();
        int enclave_destroy();
        int enclave_attest();
        int enclave_attest(uintptr_t nonce);
    }
    ```

2. Register the interface you implemented in the previous step to ./enclave/tee_framework/new_enclave/src/xxEnclaveEngine.cpp at the Register Interface tag in the EnclaveEngine constructor like penglai.
    `./enclave/tee_framework/penglai/src/PLEnclaveEngine.cpp`
    ```c++
        _enclave_init = std::bind(&PLenclave_init, std::placeholders::_1);
        _enclave_finalize = std::bind(&PLenclave_finalize, std::placeholders::_1);
        _enclave_create = std::bind(&PLenclave_create, std::placeholders::_1,
                                    std::placeholders::_2, std::placeholders::_3);
        _enclave_run = std::bind(&PLenclave_run, std::placeholders::_1);
        _enclave_stop = std::bind(&PLenclave_stop, std::placeholders::_1);
        _enclave_resume = std::bind(&PLenclave_resume, std::placeholders::_1);
        _enclave_attest = std::bind(&PLenclave_attest, std::placeholders::_1,
                                    std::placeholders::_2);
        _enclave_destroy = std::bind(&PLenclave_destroy, std::placeholders::_1);

    ```
3. Add your header and source paths in demo CMakeLists.

4. The CMake compile command specifies the parameter ENCLAVE when compiling, e.g. *-DENCLAVE=PL* when using the penglai enclave. You can replace PL with a new label of your own.

   ```
   cmake -DTHIRDPARTY=ON -DENCLAVE=PL -DCOMPILE_EXAMPLES=ON ..
   ```

5. Build and rerun the demo
