/*
 * Copyright (c) 2023 IPADS, Shanghai Jiao Tong University.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef RUN_ENCLAVE_PARAMES_H
#define RUN_ENCLAVE_PARAMES_H
#include "PLEnclaveEngine.h"
#include "face_recognition_shared_param.h"
namespace face_recognition {
void run_enclave_with_args(struct penglaienclave::Enclave *enclave);
}
#endif